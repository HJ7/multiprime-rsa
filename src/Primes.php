<?php
declare(strict_types = 1);

namespace Strife\RSA;

class Primes implements \Iterator, \ArrayAccess, \Countable
{
    private $primes = [];
    private $pointer = 0;

    public function __construct(...$primes)
    {
        foreach($primes as $prime)
        {
            Helper::gmpify($prime);

            if (gmp_prob_prime($prime) == 0) {
                throw new \Exception("'$prime' is not a prime number.");
            }

            if (array_search($prime, $this->primes) !== false) {
                throw new \Exception("Duplicate prime: $prime");
            }

            $this->primes[] = $prime;
        }

        if (($c=count($this->primes)) < 2) {
            throw new \Exception("At least two primes required, $c given.");
        }
    }

    // Iterator
    public function rewind() : void
    {
        $this->pointer = 0;
    }

    public function current() : \GMP
    {
        return $this->primes[$this->pointer];
    }

    public function key() : int
    {
        return $this->pointer;
    }

    public function next() : void
    {
        ++$this->pointer;
    }

    public function valid() : bool
    {
        return isset($this->primes[$this->pointer]);
    }

    // ArrayAccess
    public function offsetExists($offset) : bool
    {
        return isset($this->primes[$offset]);
    }

    public function offsetUnset($offset) : void
    {
        throw new \Exception("Do not use offsetUnset on this object.");
    }

    public function offsetSet($offset, $value) : void
    {
        throw new \Exception("Do not use offsetSet on this object");
    }

    public function offsetGet($offset) : \GMP
    {
        return $this->primes[$offset];
    }

    // Countable
    public function count() : int
    {
        return count($this->primes);
    }
}