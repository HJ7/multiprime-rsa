<?php
declare(strict_types = 1);

namespace Strife\RSA\Tests;

use Strife\RSA;
use PHPUnit\Framework\TestCase;

class PrimesTest extends TestCase
{
    public function testIntegerConstruction()
    {
        $a = new RSA\Primes(13207463, 13563857);
        $this->assertAttributeCount(2, 'primes', $a);
    }

    public function testStringConstruction()
    {
        $a = new RSA\Primes('13207463', '13563857');
        $this->assertAttributeCount(2, 'primes', $a);
    }

    public function testGmpConstruction()
    {
        $a = new RSA\Primes(gmp_init('13207463'), gmp_init('13563857'));
        $this->assertAttributeCount(2, 'primes', $a);
    }

    public function testMixedConstruction()
    {
        $a = new RSA\Primes(15263387, '13207463', gmp_init('13563857'));
        $this->assertAttributeCount(3, 'primes', $a);
    }

    public function testBadParamCount1()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("At least two primes required, 1 given.");
        new RSA\Primes(13207463);
    }

    public function testBadParamCount2()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("At least two primes required, 0 given.");
        new RSA\Primes();
    }

    public function testNotAPrimeNumber()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("'1234' is not a prime number.");
        new RSA\Primes(13207463, 1234, 13563857);
    }

    public function testDuplicatePrime()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Duplicate prime: 13207463");
        new RSA\Primes(13207463, 13563857, 13207463);
    }

    public function testArrayAccessOffsetGetAndExists()
    {
        $a = new RSA\Primes(13207463, 13563857);

        $this->assertEquals('13207463', (string)$a[0]);
        $this->assertEquals('13563857', (string)$a[1]);
        $this->assertArrayNotHasKey(2, $a);
    }

    public function testArrayAccessOffsetSet()
    {
        $a = new RSA\Primes(13207463, 13563857);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Do not use offsetSet on this object");
        $a[0] = 1;
    }

    public function testArrayAccessOffsetUnset()
    {
        $a = new RSA\Primes(13207463, 13563857);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Do not use offsetUnset on this object");
        unset($a[0]);
    }

    public function testCountableCount()
    {
        $a = new RSA\Primes(13207463, 13563857);
        $this->assertEquals(2, count($a));
    }

    public function testIteratorWithArrayAccess()
    {
        $a = new RSA\Primes(15263387, 13207463, 13563857);

        foreach($a as $b => $c) {
            $this->assertEquals($c, $a[$b]);
        }
    }
}