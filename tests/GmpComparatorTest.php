<?php
declare(strict_types = 1);

namespace Strife\RSA\Tests;

use Strife\RSA;
use PHPUnit\Framework\TestCase;

class GmpComparatorTest extends TestCase
{
    /**
     * @group meta
     */
    function testSanity1()
    {
        $a = gmp_init('1');
        $b = gmp_init('1');

        $this->assertEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity2()
    {
        $a = gmp_init('1');
        $b = '1';

        $this->assertEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity3()
    {
        $a = '1';
        $b = gmp_init('1');

        $this->assertEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity4()
    {
        $a = gmp_init('1');
        $b = gmp_init('2');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity5()
    {
        $a = gmp_init('1');
        $b = '2';

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity6()
    {
        $a = '1';
        $b = gmp_init('2');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity7()
    {
        $a = 1;
        $b = gmp_init('1');

        $this->assertEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity8()
    {
        $a = gmp_init('1');
        $b = 1;

        $this->assertEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity9()
    {
        $a = 1.0;
        $b = gmp_init('1');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity10()
    {
        $a = gmp_init('1');
        $b = 1.0;

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity11()
    {
        $a = '1.0';
        $b = gmp_init('1');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity12()
    {
        $a = gmp_init('1');
        $b = '1.0';

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity13()
    {
        $a = '0x1';
        $b = gmp_init('1');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity14()
    {
        $a = gmp_init('1');
        $b = '0x1';

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity15()
    {
        $a = '0b1';
        $b = gmp_init('1');

        $this->assertNotEquals($a, $b);
    }

    /**
     * @group meta
     */
    function testSanity16()
    {
        $a = gmp_init('1');
        $b = '0b1';

        $this->assertNotEquals($a, $b);
    }
}