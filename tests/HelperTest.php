<?php
declare(strict_types = 1);

namespace Strife\RSA\Tests;

use Strife\RSA;
use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
    public function testCanGmpifyInteger()
    {
        $a = 1;
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyLargestNativeInteger()
    {
        $a = PHP_INT_MAX;
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyNegativeInteger()
    {
        $a = -1;
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpityLargestNativeNegativeInteger()
    {
        $a = PHP_INT_MIN;
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyNumericString()
    {
        $a = '1';
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyLargeNumericString()
    {
        // 2**1024
        $a = '179769313486231590772930519078902473361797697894230657273430081157732675805500963132708477322407536021120113879871393357658789768814416622492847430639474124377767893424865485276302219601246094119453082952085005768838150682342462881473913110540827237163350510684586298239947245938479716304835356329624224137216';
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyNegativeNumericString()
    {
        $a = '-1';
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyLargeNegativeNumericString()
    {
        $a = '-179769313486231590772930519078902473361797697894230657273430081157732675805500963132708477322407536021120113879871393357658789768814416622492847430639474124377767893424865485276302219601246094119453082952085005768838150682342462881473913110540827237163350510684586298239947245938479716304835356329624224137216';
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCanGmpifyGmpObject()
    {
        $a = gmp_init(1);
        $b = RSA\Helper::gmpify($a);
        $this->assertInstanceOf('GMP', $a);
        $this->assertTrue($b);
    }

    public function testCannotGmpifyFloat()
    {
        $a = 1.2;
        $b = gettype($a);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Unsupported data type '$b' passed to gmpify.");
        RSA\Helper::gmpify($a);
    }

    public function testCannotGmpifyBool()
    {
        $a = true;
        $b = gettype($a);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Unsupported data type '$b' passed to gmpify.");
        RSA\Helper::gmpify($a);
    }

    public function testCannotGmpifyUnknownObject()
    {
        $a = (object)'a';
        $b = gettype($a) . ' of ' . get_class($a);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Unsupported data type '$b' passed to gmpify.");
        RSA\Helper::gmpify($a);
    }

    public function testCannotGmpifyUnknownClass()
    {
        $a = new class {};
        $b = gettype($a) . ' of ' . get_class($a);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Unsupported data type '$b' passed to gmpify.");
        RSA\Helper::gmpify($a);
    }
}