<?php
declare(strict_types = 1);

require_once __DIR__ . '/../vendor/autoload.php';


SebastianBergmann\Comparator\Factory::getInstance()->register(
    new \Strife\RSA\Tests\GmpComparator()
);
