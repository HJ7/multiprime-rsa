Multiprime RSA
==============

Usage
-----
See `tests/RSATest.php` for usage because I'm too lazy to describe it here.

Notes
-----
**Not intended for cryptographic purposes**, do not use in production! This is plain RSA, no padding is used.

PHPUnit
-------
Bootstrap required: `phpunit --bootstrap tests/bootstrap.php tests/`

### Groups
* slow
  * Tests which are likely to perform rather slow.
* meta
  * Tests testing comparators, such as `tests/GmpComparatorTest.php`.
* silly
  * Tests which don't necessarily cover intended features, such as `tests/RSATest.php testMultiplicativeHomomorphism()`

License
=======
    Multiprime RSA, written in PHP using GMP. Encrypt and decrypt RSA
    with any amount of primes.
    Copyright (C) 2016-2017  Henry Jekyll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.