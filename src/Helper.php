<?php
declare(strict_types = 1);

namespace Strife\RSA;

class Helper
{
    public static function gmpify(&$number) : bool
    {
        if ($number instanceof \GMP) {
            return true;
        }

        if (is_int($number) || is_string($number)) {
            $tmp = $number;
            $number = @gmp_init($number, 10);
            if ($number === false) {
                throw new \Exception("'$tmp' is not a valid decimal number.");
            }
            return true;
        }

        $t = gettype($number);
        if ($t == 'object') {
            $t .= ' of ' . get_class($number);
        }

        throw new \Exception("Unsupported data type '$t' passed to gmpify.");
    }
}