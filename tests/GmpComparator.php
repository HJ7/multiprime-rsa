<?php
declare(strict_types = 1);

namespace Strife\RSA\Tests;

use SebastianBergmann\Comparator;

class GmpComparator extends Comparator\Comparator
{
    public function accepts($expected, $actual)
    {
        if ($expected instanceof \GMP && $actual instanceof \GMP) {
            return true;
        }

        if ($expected instanceof \GMP) {
            $a = @gmp_init($actual, 10);
            if ($a !== false) {
                return true;
            }
        }

        if ($actual instanceof \GMP) {
            $a = @gmp_init($expected, 10);
            if ($a !== false) {
                return true;
            }
        }

        return false;
    }

    public function assertEquals($expected, $actual, $delta = 0, $canonicalize = false, $ignoreCase = false)
    {
        if (is_float($delta) || (int)$delta != $delta) {
            $delta = (int)$delta;
        }

        if (gmp_abs($expected - $actual) > $delta) {
            throw new Comparator\ComparisonFailure(
                $expected,
                $actual,
                (string)$expected,
                (string)$actual
            );
        }
    }
}