<?php
declare(strict_types = 1);

namespace Strife\RSA;

class RSA
{
    private $e;
    private $r;
    private $n;
    private $d;
    private $t;

    public function __construct($publicExponent, Primes $primes)
    {
        Helper::gmpify($publicExponent);

        if ($publicExponent < 3) {
            throw new \Exception("Public exponent must be an integer greater than 2.");
        }

        $this->e = $publicExponent;
        $this->r = $primes;

        $this->initKey();
    }

    private function initKey() : void
    {
        $k = count($this->r);
        $this->n = 1;
        Helper::gmpify($this->n);
        for($i = 0; $i < $k; $i++) {
            $this->n *= $this->r[$i];
            $this->d[$i] = gmp_invert($this->e, $this->r[$i] - 1);

            if ($this->d[$i] === false) {
                throw new \Exception("No inverse modulo $this->e, {$this->r[$i]} - 1");
            }
        }

        $m = $this->r[0];
        for($i = 1; $i < $k; $i++) {
            $this->t[$i] = gmp_invert($m, $this->r[$i]);

            if ($this->t[$i] === false) {
                throw new \Exception("No inverse modulo $m, {$this->r[$i]}");
            }

            $m *= $this->r[$i];
        }
    }

    public function encrypt($message) : \GMP
    {
        Helper::gmpify($message);

        if (gmp_sign($message) == -1) {
            throw new \Exception("Input message is negative.");
        }

        if ($message >= $this->n) {
            throw new \Exception("Input message is too big for public key.");
        }

        return gmp_powm($message, $this->e, $this->n);
    }

    public function decrypt($ciphertext) : \GMP
    {
        Helper::gmpify($ciphertext);

        $k = count($this->r);
        $x = [];
        for($i = 0; $i < $k; $i++) {
            $x[$i] = gmp_powm($ciphertext % $this->r[$i], $this->d[$i], $this->r[$i]);
        }

        $z = $x[0];
        $m = $this->r[0];
        for($i = 1; $i < $k; $i++) {
            $z += $m * (($x[$i] - $z % $this->r[$i]) * $this->t[$i] % $this->r[$i]);
            $m *= $this->r[$i];
        }

        return $z;
    }
}